// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.

// [SECTION] Inserting Documents (CREATE)

/*
 Syntax:
    -db.collectionName.insertOne({object});
*/


// Insert One

// Whenever we insert documents we are creating new collections 
db.users.insert({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "87654321",
        email: "janedoe@gmail.com"
    },
    courses: ["CSS", "JavaScript", "Python"],
    department: "none"
});


// Insert Many 
db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawkings",
        age: 76,
        contact: {
            phone: "9876321",
            email: "stephenhawking@gmail.com"
    },
    courses: ["Python", "React", "PHP"],
    department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87694561",
            email: "neil@gmail.com"
    },
    courses: ["React", "Laravel", "MongoDB"],
    department: "none"
    }
]);


// [SECTION] Finding Documents (READ)
/*
 db.collectionName.find();
 db.collectionName.find({field: value});
*/

db.users.find();
db.users.find({firstName: "Stephen"});
db.users.find({lastName: "Armstrong", age: 82});


// -----------------------
db.users.insert({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});


// [SECTION] Updating documents (UPDATE)

db.users.updateOne(
    {firstName: "Test"},
    {
        $set: {
            firstName: "Bill",
            lastName:"Gates",
            age: 65,
            contact: {
                phone: "12345678",
                email: "bill@mail.com"
            },
            courses: ["PHP", "Larave", "HTML"],
            department: "Operations",
            status: "active"
            }
        }
)



// Updating multiple documents 
/*
    syntax: 
        db.collectionName.updateMany({criteria}, {$set: {field: value}});
*/


db.users.updateMany(
    {department: "none"},
    {
        $set: {
            department:"HR"
        }
    });


// Replace One 
/*
    can be used if replacing the whole document is necessary
*/

db.users.replaceOne(
    {firstName: "Bill"},
    {
        firstName: "Billy",
        lastName: "Crawford",
        age: 30,
        contact:{
            phone: "12345678",
            email: "billy@mail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "active"
    }
);



// -----------------------
/*
    db.collectionNAme.deleteOne({Critera/})
*/

db.users.deleteOne({
    firstName: "Stephen");



// Delete Many
db.users.deleteMany({
    firstName: "Jane");


